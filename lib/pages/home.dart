import 'package:artisapp/models/items.dart';
import 'package:artisapp/pages/album.dart';
import 'package:artisapp/provider/data_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';




  Widget FutureArtist() {
    return FutureBuilder(
      future: menuProvider.cargarDataArtistas(),
      //initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot) {
         return CarouselSlider(
                      options: CarouselOptions(
                        autoPlay: true,
                        aspectRatio: 2.0,
                        enlargeCenterPage: true,
                      ),
                      
                      items: ArtistList(context, snapshot.data)
                    );
      },
    );
 
  }

  

  Widget FutureAlbums() {
    return FutureBuilder(
      future: menuProvider.cargarDataAlbumes(),
      //initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot) {
         return CarouselSlider(
                      options: CarouselOptions(
                        autoPlay: true,
                        aspectRatio: 2.0,
                        enlargeCenterPage: true,
                      ),
                      
                      items: AlbumesList(context, snapshot.data)
                    );
      },
    );
 
  }
List<Widget> ArtistList(BuildContext context, List<dynamic>? artistas) {
  final List<Widget> imageSlidersArtistas = [];
  artistas?.forEach((element) {
    final Widget imageSlider = GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/artist', arguments: ScreenArguments(element));
          },
            child: Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(element['profile']),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          element['name'],
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));

    imageSlidersArtistas.add(imageSlider);
  });
  return imageSlidersArtistas;
}

List<Widget> AlbumesList(BuildContext context, List<dynamic>? albumes) {
  final List<Widget> imageSlidersAlbumes = albumes!
      .map(
        (item) => GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/album', arguments: ScreenArguments(item));
            },
            child: Container(
              child: Container(
                margin: EdgeInsets.all(5.0),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(item['cover']),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromARGB(200, 0, 0, 0),
                                  Color.fromARGB(0, 0, 0, 0)
                                ],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                              ),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 20.0),
                            child: Text(
                              item['name'],
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ),
            )),
      )
      .toList();

  return imageSlidersAlbumes;
}


class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    FutureAlbums();
    return Scaffold(
        appBar: AppBar(title: Text('ArtistApp')),
        body: Column(
          children: [
            Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Todos los Artistas",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: "CoralPen",
                          fontSize: 30.0,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: FutureArtist(),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Todos los Albumes",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: "CoralPen",
                          fontSize: 30.0,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: FutureAlbums()
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}