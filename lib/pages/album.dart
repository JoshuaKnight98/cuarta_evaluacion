
import 'package:artisapp/models/items.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './music_player_screen.dart';

GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

class AlbumScreen extends StatefulWidget {
  @override
  _AlbumScreenState createState() => _AlbumScreenState();
}

class _AlbumScreenState extends State<AlbumScreen> {
  List<Song> listSong = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    final data = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    initListSong(data);
    return Scaffold(
      key: scaffoldState,
      body: Stack(
        children: <Widget>[
          _buildWidgetAlbumCover(mediaQuery, data),
          _buildWidgetActionAppBar(mediaQuery),
          _buildWidgetFloatingActionButton(mediaQuery),
          _buildWidgetListSong(mediaQuery, data),
        ],
      ),
    );
  }

  Widget _buildWidgetListSong(MediaQueryData mediaQuery, data) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.0,
        top: mediaQuery.size.height / 1.8 + 48.0,
        right: 20.0,
        bottom: mediaQuery.padding.bottom + 16.0,
      ),
      child: Column(
        children: <Widget>[
          _buildWidgetHeaderSong(data),
          SizedBox(height: 16.0),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.zero,
              separatorBuilder: (BuildContext context, int index) {
                return Opacity(
                  opacity: 0.5,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Divider(
                      color: Colors.grey,
                    ),
                  ),
                );
              },
              itemCount: data.item['songs'].length,
              itemBuilder: (BuildContext context, int index) {
                Song song = listSong[index];
                return GestureDetector(
                  onTap: () {
                    print(song.cover);
                    _navigatorToMusicPlayerScreen(song.title, song.artist, song.cover, song.lyrics);
                  },
                  child: Row(
                    children: <Widget>[
                       Icon(
                          Icons.music_note,
                          color: Colors.grey,
                        ),
                      Expanded(
                        child: Text(
                          song.title,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontFamily: "Campton_Light",
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Text(
                        song.duration,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(width: 24.0),
                      Icon(
                        Icons.more_horiz,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetHeaderSong(data) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
        data.item['name'],
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 24.0,
            fontFamily: "Campton_Light",
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetFloatingActionButton(MediaQueryData mediaQuery) {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.only(
          top: mediaQuery.size.height / 1.8 - 32.0,
          right: 32.0,
        ),
        child: FloatingActionButton(
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
          ),
          backgroundColor: Color(0xFF7D9AFF),
          onPressed: () {
            _navigatorToMusicPlayerScreen(listSong[0].title, listSong[0].artist, listSong[0].cover, listSong[0].lyrics);
          },
        ),
      ),
    );
  }

  void _navigatorToMusicPlayerScreen(String title,  String artist, String cover, String lyrics) {
    Navigator.of(scaffoldState.currentContext!)
        .push(MaterialPageRoute(builder: (context) {
      return MusicPlayerScreen(title, artist, cover, lyrics);
    }));
  }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: mediaQuery.padding.top + 16.0,
        right: 16.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[        
        ],
      ),
    );
  }

  Widget _buildWidgetAlbumCover(MediaQueryData mediaQuery, data) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 1.8,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        
        image: DecorationImage(
          image:
              AssetImage(data.item['cover']),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  void initListSong(data) {
    data.item['songs'].forEach((song) {
      listSong.add(Song(
        title: song['name'],
        duration: song['duration'],
        artist: data.item['name'],
        cover: data.item['cover'],
        lyrics: song['lyrics'],
      ));
    });
 
  }
}

class Song {
  String title;
  String duration;
  String artist;
  String cover;
  String lyrics;

  Song({required this.title, required this.duration, required this.artist, required this.cover, required this.lyrics});

  @override
  String toString() {
    return 'Song{title: $title, duration: $duration}';
  }
}
